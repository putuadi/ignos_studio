<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BookingController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get("/", [BookingController::class, "index"])->name("index");
Route::get("/booking", [BookingController::class, "booking"])->name("booking");
Route::post("/storeBooking", [BookingController::class, "store"])->name("storeBookings");
Route::get("/timeBooking/{date}", [BookingController::class, "time"])->name("timeBookings");
