<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookingController extends Controller
{
    public function index()
    {
        return view("home");
    }

    public function booking()
    {
        return view("booking");
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            "nama" => "required|max:50",
            "jumlah_orang" => "required|integer|between:1,5",
            "tanggal" => "required|date|after:today|max:50",
            "waktu" => "required|max:50",
            "package" => "required|max:50",
            "background" => "required|max:50",
        ]);

        if ($validator->fails()) {
            return redirect()->route("bookings")->withErrors($validator)->withInput();
        }

        $existingBooking = Booking::where('tanggal', $request->tanggal)
            ->where('waktu', $request->waktu)
            ->exists();

        if ($existingBooking) {
            return redirect()->route("bookings")->withErrors(["waktu" => "Waktu sudah ada yang memesan."])->withInput();
        } else {
            Booking::create($request->all());

            $message = "FORM BOKING \n\nNama : $request->nama \nJumlah Orang : $request->jumlah_orang \nWaktu Booking : $request->tanggal / $request->waktu \nPackage : $request->package \nBackground : $request->background \n\nDetail Lokasi : \nJln. Raya Gerih Blumbungan, Sibang Kaja, Abiansemal, Badung(https://maps.app.goo.gl/8teVU4jD3afBMNfX7?g_st=iw) \n\nNote : \n- Mohon untuk datang tepat waktu(lebih baik 15 menit sebelum jam booking), apabila terlambat mohon maaf kami tidak ada penambahan waktu. \n\nTerimakasih sudah reservasi, see you kak \u{2728}\u{1F60A}";

            $whatsappLink = 'https://wa.me/6281529751265?text=' . urlencode($message);

            echo "
                <script>
                    var whatsappLink = '$whatsappLink';
                    window.open(whatsappLink, '_self');
                </script>
            ";
        }
    }
    public function time($date)
    {
        $data = Booking::where("tanggal", $date)->get();
        for ($i = 8; $i <= 22; $i++) {
            $nama = ($i < 10) ? "0$i:00" : "$i:00";
            $i2 = $i + 1;
            $nama2 = ($i2 < 10) ? "0$i2:00" : "$i2:00";

            $nama = $nama . "-" . $nama2;
            $found = false;
            foreach ($data as $x) {
                if ($nama == $x->waktu) {
                    $found = true;
                    break;
                }
            }
            if (!$found) {
                echo "
                    <div>
                        <input class=\"form-check-input\" type=\"radio\" name=\"waktu\" id=\"$i\" value=\"$nama\" required>
                        <label class=\"form-check-label\" for=\"$i\">
                            $nama
                        </label>
                    </div>
                ";
            }
        }
    }
}
